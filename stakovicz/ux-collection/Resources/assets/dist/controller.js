'use strict';

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _stimulus = require("stimulus");

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var _default = /*#__PURE__*/function (_Controller) {
  _inherits(_default, _Controller);

  var _super = _createSuper(_default);

  function _default() {
    var _this;

    _classCallCheck(this, _default);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _super.call.apply(_super, [this].concat(args));

    _defineProperty(_assertThisInitialized(_this), "index", 0);

    _defineProperty(_assertThisInitialized(_this), "controllerName", null);

    return _this;
  }

  _createClass(_default, [{
    key: "connect",
    value: function connect() {
      this.controllerName = this.context.scope.identifier;
      var dataset = this.context.scope.element.dataset;

      if (1 == dataset.allowAdd) {
        // Add button Add
        var buttonAdd = this._textToNode('<button data-action="' + this.controllerName + '#add"' + ' class="' + dataset.buttonAddClass + '" type="button">' + dataset.buttonAddText + '</button>');

        this.containerTarget.prepend(buttonAdd);
      } // Add buttons Delete


      if (1 == dataset.allowDelete) {
        for (var i = 0; i < this.entryTargets.length; i++) {
          this.index = i;
          var entry = this.entryTargets[i];

          this._addDeleteButton(entry, this.index);
        }
      }
    }
  }, {
    key: "add",
    value: function add(event) {
      this.index++; // Compute the new entry

      var newEntry = this.containerTarget.dataset.prototype;
      newEntry = newEntry.replace(/__name__label__/g, this.index);
      newEntry = newEntry.replace(/__name__/g, this.index);
      newEntry = this._textToNode(newEntry);
      newEntry = this._addDeleteButton(newEntry, this.index);
      this.containerTarget.append(newEntry);
    }
  }, {
    key: "delete",
    value: function _delete(event) {
      var theIndexEntryToDelete = event.target.dataset.indexEntry; // Search the entry to delete from the data-index-entry attribute

      for (var i = 0; i < this.entryTargets.length; i++) {
        var entry = this.entryTargets[i];

        if (theIndexEntryToDelete === entry.dataset.indexEntry) {
          entry.remove();
        }
      }
    }
    /**
     * Add the delete button to the entry
     * @param String entry
     * @param Number index
     * @returns {ChildNode}
     * @private
     */

  }, {
    key: "_addDeleteButton",
    value: function _addDeleteButton(entry, index) {
      // link the button and the entry by the data-index-entry attribute
      entry.dataset.indexEntry = index;
      var dataset = this.context.scope.element.dataset;

      var buttonDelete = this._textToNode('<button data-action="' + this.controllerName + '#delete"' + ' data-index-entry="' + index + '" class="' + dataset.buttonDeleteClass + '" type="button">' + dataset.buttonDeleteText + '</button>');

      entry.append(buttonDelete);
      return entry;
    }
    /**
     * Convert text to Element to insert in the DOM
     * @param String text
     * @returns {ChildNode}
     * @private
     */

  }, {
    key: "_textToNode",
    value: function _textToNode(text) {
      var div = document.createElement('div');
      div.innerHTML = text.trim();
      return div.firstChild;
    }
  }]);

  return _default;
}(_stimulus.Controller);

exports["default"] = _default;

_defineProperty(_default, "targets", ['container', 'entry']);