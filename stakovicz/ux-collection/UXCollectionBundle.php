<?php


namespace Stakovicz\UXCollection;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * @final
 * @experimental
 */
class UXCollectionBundle extends Bundle
{
}
